<?php

use Civi\WorkflowMessage\GenericWorkflowMessage;

/**
 * @method static setPetitionNameHTML(string $html)
 * @method static setCustomThanksHTML(string $html)
 */
class CRM_Inlaypetition_WorkflowMessage_ThanksForSigning extends GenericWorkflowMessage {

  public const WORKFLOW = 'inlaypetition_thanks';

  /**
   * @var string
   * @scope tplParams
   */
  protected $petitionNameHTML;

  /**
   * @var string
   * @scope tplParams
   */
  protected $customThanksHTML;

}
