<?php

use Civi\Api4\Inlay;
use Civi\Inlay\Petition;
use Civi\Inlay\Type;
use CRM_Inlaypetition_ExtensionUtil as E;

class CRM_Inlaypetition_Page_Signatures extends CRM_Core_Page {

  public function run() {

    $inlayID = (int) ($_GET['inlayID'] ?? 0);

    if ($inlayID) {
      try {
        $inlay = Type::fromId($inlayID);
      }
      catch (\RuntimeException $e) {
        CRM_Core_Session::setStatus("Petition not found!", "Error", 'error');
      }
      if (!($inlay instanceof Petition)) {
        CRM_Core_Session::setStatus("Invalid petition ID", "Error", 'error');
        $inlay = NULL;
      }

    }
    $this->assign('when', date('H:i j M Y'));

    if (empty($inlay)) {
      // Present form.
      $this->assign('view', 'list');
      $inlays = Inlay::get()
        ->addWhere('class', '=', 'Civi\\Inlay\\Petition')
        ->addOrderBy('name', 'ASC')
        ->execute()->getArrayCopy();
      $list = [];
      foreach ($inlays as $inlayData) {
        $inlay = Type::fromArray($inlayData);
        if (($inlay->config['uxMode'] ?? '') === 'petition') {
          $list[] = ['id' => $inlayData['id'], 'name' => $inlayData['name'], 'publicTitle' => $inlay->config['publicTitle']];
        }
      }
      $this->assign('petitions', $list);
    }
    else {
      $this->assign('view', 'signatures');
      // Generate petition signatures.
      $contacts = \Civi\Api4\Contact::get()
        ->addSelect('display_name', 'activity.activity_date_time', 'address_primary.postal_code')
        ->addJoin('ActivityContact AS activity_contact', 'INNER',
          ['activity_contact.record_type_id', '=', 3],
          ['activity_contact.contact_id', '=', 'id']
        )
        ->addJoin('Activity AS activity', 'INNER',
          ['activity_contact.activity_id', '=', 'activity.id'],
          ['activity.subject', '=', json_encode($inlay->getName())],
          ['activity.activity_type_id:name', '=', '"inlay_petition"']
        )
        ->addGroupBy('id') /* The system prevents this, but humans could override */
        ->addOrderBy('activity.id', 'ASC')
        ->execute();

      // @todo farm out to an alter hook.
      $html = '<ol>';
      foreach ($contacts as $contact) {
        preg_match('/^([a-zA-Z0-9]+)/', $contact['address_primary.postal_code'], $matches);
        $postCode = $matches[1] ?? '';
        $area = preg_replace('/\[.*$/', '', OwnIt::getAreaFromPostcode($postCode));

        $line = "$contact[display_name], $postCode, $area";
        $html .= "<li>" . htmlspecialchars($line) . "</li>\n";
      }
      $html .= '</ol>';

      $this->assign('publicTitle', $inlay->config['publicTitle']);
      CRM_Utils_System::setTitle($inlay->config['publicTitle']);
      $this->assign('signatures', $html);
      $this->assign('signaturesCount', number_format($contacts->countFetched()));
    }
    parent::run();
  }

}
