# Petition "Inlay" for adding petitions to remote websites

Embed high performance petitions/signup forms that are configured in CiviCRM, in your (possibly separate) public website.

Please see [Inlay](https://lab.civicrm.org/extensions/inlay) for an introduction to the technology this is based on.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Features

- Create multiple petition/signup forms
- Include personalised links in CiviMail so recipients can sign with one click.
- Takes first, last name, email, optionally postcode.
- Two possible operation modes: Petition (includes counter, last signature etc.)
  or signup (simpler).
- Petition has choice of UI: compact (e.g. use in a sidebar) and two column
  (e.g. when it has the full page width to play with)
- All texts customisable.
- opt-in/consent UX choice: none (e.g. the whole form is for the purposes of
  signing up); Radios (forces user to choose yay or nay - known to increase yeses); Checkbox.
- Choice of UX after initial form:
  1.  Show "please share this petition" screen, then next show final thanks screen.
  2.  Show thanks message (which might, for example link to your donation page), with a "skip" link that takes you to a "please share" screen to end.
  3.  Show thanks only.
- Allows admins to provide an image for the petition.
- Social Shares: optionally uses privacy-friendly share links to Xitter/Facebook/WhatApp/Email
- Optional, re-usable thank you email templates: you can have one template and
  use it for many petitions, should you wish, and include a bit of custom content.
- Optionally add to group (if opted in). Does not remove from group if not
  opted-in.
- Optionally use a queue. Check this if your site is busy and submissions will
  be queued and processed by cron.
- Advanced: Variations allow variations on the same petition (e.g. one per
  locality) - these need programmatic implementation on your website though.
- Export signatures: Visit **Reports » Petition Signatures (Petition Inlay)**
  click the petition you want and you’ll see a plain page with just the signatures as an ordered HTML list (ordered by date signed).
- Optional UK Postcode validation, supporting regex and, if installed, Artful
  Robot’s
  [postcodesio extension](https://codeberg.org/artfulrobot/postcodesio) _not to be confused with the similarly named one in civicrm.org/extensions that I didn’t spot before naming mine_
- Javascript events allow local code to take action.

## Theming

There are a handful of rules to give a little style. You may need to override these in your site's CSS if you don't like them.

You can also set the following CSS custom properties:

- `--inlay-petition-background` main background colour.
- `--inlay-petition-padding` main padding colour.
- `--inlay-petition-bar-domain` colour of the barchart's background.
- `--inlay-petition-bar` colour of the barchart's bar.
- `--inlay-socials-radius` border-radius of social share buttons.
- `--inlay-socials-background` social share buttons.
- `--inlay-socials-background-hover` social share buttons.
- `--inlay-socials-color` social share buttons.
- `--inlay-socials-color-hover` social share buttons.

### Custom layouts

Single- and two-column layouts are included. You can also use custom layouts if you can write you own HTML + AlpineJS code. Custom layout HTML files live at `[civicrm.files]/inlaypetition/layout-N.html` where `N` is the ID of your inlay (see it in the URL when editing one, or in the UI hint where you select layout).

Look at the layout-two-column.html and layout-single-column.html for examples.

Variables used by AlpineJS attributes:

- `initData` an object containing all the public-safe data that is included in
  the front-end bundles.
- `submitForm` the function your form must call.
- `stage` one of: `form|social|final` use `changeStage()` if you need to change
  it.
- `cs` , `ctID` these will be set if personal data was successfully loaded via
  checksum in the URL fragment. You should offer a "Not you?" link which calls `removePrefillData`
- `userData` this is the model holding the form data
  - `first_name`
  - `last_name`
  - `email`
  - `email2`
  - `postcode`
  - `optin` set to `"yes"` if the user consents.
  - `submissionRunning`

## CiviMail pre-filled data links

If your petition is placed at `https://example.org/my-petition` then you can link to

    https://example.org/my-petition#{contact.checksum}~cid={contact.contact_id}

in CiviMail (and other places that handles such tokens) to email people a link that will pre-load their data.

## Hacking the processing of input data

You can tinker with or completely replace the way the data is processed by listening for the
`civi.inlaypetition.alterpipeline` event and modifying the `$event->pipeline` array.

By default there's a set of callbacks with the following keys:

- `findOrCreateContact` adds contactID to \$data
- `ensureEmailNotOnHold`
- `updatePostcode`
- `handleSignup`
- `recordActivity`
- `sendThanksEmail`

The callbacks need to have signature: `(\Civi\Inlay\Type $inlay, array &$data)`. They are called in a foreach loop.
So you can replace one with your own, or delete one. But read the docblocks first, since some processes
depend on keys in \$data added by earlier stages.

## Requirements

- PHP 8+
- CiviCRM 5.56+
- <del>The Email API extension</del>

## Installation (Web UI)

Learn more about installing CiviCRM extensions in the [CiviCRM Sysadmin Guide](https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/).

## Getting Started

Once installed, visit **Administer » Inlays** and create a Petition Inlay.

## Events (developers)

The `event.detail` includes:

- `inlay` - a reference to the inlay object. Warning: many properties and 
  methods are internal and subject to change.
- `publicTitle`
- `publicID` the aa112233dd type ID.
- `uxMode` either `petition` or `signup`

- `inlaypetitionsigned` is fired on successful submission. 
  `optIn` is also included in the detail object and is "yes" or "no"
- `inlaypetitionshared` is fired if socials are configured and one of the 
  buttons is clicked. `event.detail` also includes  `socialNetworkName` (string)
- `inlaypetitionskippedsocial` is fired if socials are configured and Skip is 
  clicked.


## Changes

### Version 2.5

- ❇ Emit Javascript events in various situations, allowing integration 
  with, for example, privacy-friendly analytics. If you have custom templates you will
  need to update these to benefit from the events - see changes to built-in layouts.

### Version 2.4

- 🐞 Fix bug that prevented submission of single-column layout forms due to 
  mis-matching emails.

### Version 2.3

- ❇ Add support for custom layouts
- ❇ Add support for Mastodon, Threads, Bluesky, update Xitter logo.
- ❇ Add support for Artful Robot’s postcodesio extension for UK postcode
  formatting/validation.
- 🐞 Fix bug that resulted in duplicate addresses.

### Version 2.2

- Bug fix/enhancement of up-to-date counts.

### Version 2.1

- Bug fix: after 2 minutes the recount would cause the bar chart to go huge,
  likely causing the browser to shrink the page to fit it, making it impossible
  to sign.

### Version 2

- Different templates, one includes optional image.
- Check-sum links (from CiviMail, append
  `#{contact.checksum}~cid={contact.contact_id}` to your links)
- Rewritten in AlpineJs + web components
- No longer uses EmailAPI extension
- Added postcode, custom thank you email content, last signature date and name,
  optional double email entry.

### Version 1.3 (unreleased)

- Now logs when setting an email `is_bulkmail` or un-holding the email. Also
  sets `reset_date` when un-holding.

### Version 1.2

- Previously if a queued item failed, the queue stopped processing. This could mean a big backlog if there's one item that crashed the process. Now if a queued item fails it's moved to a `inlay-petition-failed` queue and the error is logged, and it moves onto the next item. There is no queue processor for the failed queue, so you might need to keep an eye on the logs.

- Any `on_hold` will be cleared for the given email. And it will be set to
  `is_bulkmail`
