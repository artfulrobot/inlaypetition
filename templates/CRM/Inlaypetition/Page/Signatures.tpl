{if $view=="list"}
  <p>Please choose a petition</p>
  <ul>
  {foreach from=$petitions item="petition"}
    <li><a href="?inlayID={$petition.id}">{$petition.name}</a> <em>{$petition.publicTitle}</em></li>
  {/foreach}
  </ul>
  <p><small>{$when}</small></p>

{else if $view=='signatures'}
  <p>{$signaturesCount} signatures at {$when}</p>
  {$signatures}
{/if}
