<?php

require_once 'inlaypetition.civix.php';
// phpcs:disable
use CRM_Inlaypetition_ExtensionUtil as E;
use Symfony\Component\DependencyInjection\Definition;

// phpcs:enable

/**
 * Implements hook_civicrm_container().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_container/
 */
function inlaypetition_civicrm_container($container) {
  // https://docs.civicrm.org/dev/en/latest/hooks/usage/symfony/
  //Civi::dispatcher()
  $container->findDefinition('dispatcher')
    ->addMethodCall('addListener', ['hook_inlay_registerType', [Civi\Inlay\Petition::class, 'register']]);
  $container->setDefinition("cache.inlaypetition", new Definition(
  'CRM_Utils_Cache_Interface',
  [[
    'type' => ['*memory*', 'SqlGroup'],
    'name' => 'InlayPetition',
  ],
  ]
  ))->setFactory('CRM_Utils_Cache::create')->setPublic(TRUE);

}

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function inlaypetition_civicrm_config(&$config) {
  _inlaypetition_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function inlaypetition_civicrm_install() {
  _inlaypetition_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function inlaypetition_civicrm_enable() {
  _inlaypetition_civix_civicrm_enable();
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 */
//function inlaypetition_civicrm_preProcess($formName, &$form) {
//
//}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function inlaypetition_civicrm_navigationMenu(&$menu) {
  _inlaypetition_civix_insert_navigation_menu($menu, 'Reports', [
    'label' => E::ts('Petition Signatures (Petition Inlay)'),
    'name' => 'inlaypetition_signatures',
    'url' => 'civicrm/inlaypetition/signatures',
    'permission' => 'access CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _inlaypetition_civix_navigationMenu($menu);
}
