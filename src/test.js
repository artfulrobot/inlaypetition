const s = document.currentScript;
document.addEventListener("DOMContentLoaded", () => {
  // Alpine might have loaded before we get to run, or we might need to load it.
  function loadAlpineAndBoot() {
    if (!window.Alpine) {
      const alpScr = document.createElement("script");
      alpScr.addEventListener("load", boot);
      alpScr.src = "https://unpkg.com/alpinejs";
      document.body.append(alpScr);
      console.log("should be loading Alpine", alpScr);
    } else {
      console.log("window.Alpine exists", window.Alpine);
      boot();
    }
  }

  // Now we have Alpine, we can boot our app.
  function boot() {
    console.log("Alpine has loaded");

    s.insertAdjacentHTML(
      "afterend",
      `
  <div id=root x-data="CiviCRMInlay.inlays['aabbccddee']" >
    <p>This is generated in js for <span x-text="name"></span></p>
    <p x-show="ok">ok</p>
    <p x-show="!ok">not ok</p>
    <button x-on:click="ok = !ok" >Toggle</button>
  </div>
    `
    );
  }

  window.CiviCRMInlay = {
    inlays: {
      aabbccddee: {
        name: "Gerry",
        ok: true,
        init: () => {
          console.log("alpine's init2 is running", this);
        }
      }
    }
  };
  loadAlpineAndBoot();
});
