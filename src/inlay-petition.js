(() => {
  const FIRST_DELAY_TILL_UPDATE_S = 1;
  const UPDATE_EVERY_S = 20;
  if (!window.inlayPetitionInit) {
    CiviCRMInlay.debug(
      "inlayPetitionInit 1 does not exist, will create it now"
    );
    // This is the first time this *type* of Inlay has been encountered.
    // We need to define anything global here.

    // Create the boot function.
    window.inlayPetitionInit = async (inlay) => {
      const ukPostcodeRegex = new RegExp(
        "^(GIR 0AA)|((([A-Z][0-9][0-9]?)|(([A-Z][A-HJ-Y][0-9][0-9]?)|(([A-Z][0-9][A-Z])|([A-Z][A-HJ-Y][0-9]?[A-Z])))) [0-9][A-Z]{2})$"
      );
      let tempDiv = document.createElement("div"),
        convertTextToHtml = (unsafeText) => {
          tempDiv.textContent = unsafeText;
          return tempDiv.innerHTML;
        };

      // Create Alpine's data object, which includes all its methods.
      Object.assign(inlay, {
        ometer: {
          step: 0,
          animatedCountFrom: 0,
          animatedCount: 0,
          style: { width: "10px" },
          interval: false,
        },
        submissionRunning: false,
        userData: {
          first_name: "",
          last_name: "",
          email: "",
          email2: "",
          optin: "",
        },
        variation: "",
        stage: "form", // form|social|final
        cs: null,
        ctID: null,
        lastSigner: "",
        target: 100,
        upToDateTimeout: false,

        // Init is called by Alpine.
        init() {
          CiviCRMInlay.debug("inlay.init (alpine) running", this.target);

          // Calculate suitable target. 70% seems to be a motivational way to represent a successful looking petition.
          if (this.initData.count > 0) {
            let chunk = 10 ** Math.floor(Math.log10(this.initData.count));
            this.target = Math.max(
              100,
              chunk * (Math.floor(this.initData.count / 0.7 / chunk) + 1)
            );
          }
          if (this.initData.allowVariations && this.script.dataset.variation) {
            this.handleVariations();
          }

          this.updateLastSignerTextFromInitData();

          const urlInput = new URLSearchParams(
            window.location.hash.replace(/^#/, "").replaceAll("~", "&")
          );
          if (urlInput.get("cs") && urlInput.get("cid")) {
            inlay
              .request({
                method: "post",
                body: {
                  need: "prefillData",
                  sourceURL: window.location.href.replace(/#.*$/, ""),
                  cs: urlInput.get("cs"),
                  ctID: urlInput.get("cid"),
                },
              })
              .then((r) => {
                if (r.inputs) {
                  ["first_name", "last_name", "email", "postcode"].forEach(
                    (f) => {
                      this.userData[f] = r.inputs[f] || "";
                    }
                  );
                  this.userData.email2 = r.inputs.email || "";
                  this.cs = urlInput.get("cs");
                  this.ctID = urlInput.get("cid");
                  // Because the opt-in is opt-in and not 'remove consent'
                  // we set this to 'no' which means no changes.
                  this.userData.optin = "no";
                }
                // Remove the PII from the URL to make it harder to accidentally share it.
                history.replaceState(
                  "",
                  document.title,
                  window.location.href.replace(/#.*$/, "")
                );
              });
          }
          // Do an initial reload of signers.
          if (!this.upToDateTimeout && this.initData.uxMode === "petition") {
            this.upToDateTimeout = setTimeout(
              () => this.checkUpToDate(),
              FIRST_DELAY_TILL_UPDATE_S * 1000
            );
          }
        },
        updateLastSignerTextFromInitData() {
          // Format last signer from initData.lastSigner (obj) to this.lastSigner (text)
          if (this.initData.lastSigner) {
            let x =
                Math.ceil(new Date().getTime() / 1000) -
                this.initData.lastSigner.when,
              unit = "s";

            if (x > 60 * 60 * 24) {
              x = Math.floor(x / 60 / 60 / 24);
              unit = x > 1 ? "days" : "day";
            } else if (x > 60 * 60) {
              x = Math.floor(x / 60 / 60);
              unit = x > 1 ? "hours" : "hour";
            } else if (x > 60) {
              x = Math.floor(x / 60);
              unit = x > 1 ? "mins" : "min";
            }
            this.lastSigner = `Last signed by ${this.initData.lastSigner.who} ${x} ${unit} ago`;
            CiviCRMInlay.debug(
              "updateLastSignerTextFromInitData set lastSigner",
              this.lastSigner
            );
          }
        },
        async checkUpToDate() {
          CiviCRMInlay.debug(
            `Petition count is over ${UPDATE_EVERY_S}s old, reloading.`
          );
          let newCount;
          try {
            newCount = await inlay.request({
              method: "post",
              body: { need: "upToDateCount" },
            });
          } catch (e) {
            // Fail silently.
            console.error("Petition recount failed", e);
            return;
          }
          CiviCRMInlay.debug("Petition recounted", newCount);
          if ("count" in newCount) {
            const { count, countedAt, lastSigner } = newCount;
            CiviCRMInlay.debug(
              "recount updating initData.countedAt and lastSigner",
              { count, lastSigner, obj: this }
            );
            // Update the words 'sowenso, x minutes ago' This should always change as time moves.
            Object.assign(this.initData, { countedAt, lastSigner });
            this.updateLastSignerTextFromInitData();

            // Only animate if the count increased.
            if (count > this.initData.count) {
              CiviCRMInlay.debug("Count increased from ", this.initData.count);
              // Update count in initData.
              this.initData.count = count;
              if (count > this.target) {
                // hack for early signers.
                this.target = this.target * 2;
              }

              this.startAnimation();
            } else {
              CiviCRMInlay.debug(
                "Count not increased from ",
                this.initData.count
              );
            }
          }
          // check again later.
          this.upToDateTimeout = setTimeout(
            () => this.checkUpToDate(),
            1000 * UPDATE_EVERY_S
          );
        },

        validatePostcode() {
          if (this.initData.askPostcode !== "uk") {
            return;
          }
          let error = "Invalid postcode";
          let postcode = (this.userData.postcode || "")
            .trim()
            .toUpperCase()
            .replaceAll(/  +/g, " ");
          if (!postcode) {
            error = "Postcode is required";
          } else if (postcode === "N/A" || ukPostcodeRegex.test(postcode)) {
            error = "";
          } else {
            // if the user entered multiple spaces, try to match it with every combination.
            const parts = postcode.split(" ");
            if (parts.length > 2) {
              for (let i = 1; error && i < parts.length; i++) {
                const candidate =
                  parts.slice(0, i).join("") + " " + parts.slice(i).join("");
                if (ukPostcodeRegex.test(candidate)) {
                  postcode = candidate;
                  error = "";
                }
              }
            }
            if (error) {
              // Hmm. Try inserting a space at every possible place until we have a match.
              let withoutSpaces = postcode.replaceAll(" ", "");
              for (let i = 1; error && i < withoutSpaces.length; i++) {
                const candidate =
                  withoutSpaces.substring(0, i) +
                  " " +
                  withoutSpaces.substring(i);
                if (ukPostcodeRegex.test(candidate)) {
                  postcode = candidate;
                  error = "";
                }
              }
            }
          }
          if (this.userData.postcode !== postcode) {
            this.userData.postcode = postcode;
          }
          this.$refs.postcode.setCustomValidity(error);
          this.$refs.postcode.reportValidity();
        },

        startAnimation() {
          this.ometer.start = null;
          window.requestAnimationFrame(this.animation.bind(this));
        },

        handleVariations() {
          try {
            let variation = JSON.parse(this.script.dataset.variation);
            CiviCRMInlay.debug(
              "InlayPetition successfully parsed variation",
              variation
            );
            // variation is an object that can contain the following keys.
            // {
            //   name: 'required string identifer',
            //   replacements: [[old, new], ...],
            //   initData: { ... }
            // }
            if (typeof variation !== "object") {
              throw "Variation not an object: " + typeof variation;
            }
            if (!("name" in variation)) {
              throw "Variation missing name.";
            }
            this.variation = variation.name;

            if (Array.isArray(variation.replacements)) {
              this.variationReplacements = variation.replacements;
            }
            if (variation.initData) {
              for (const prop in variation.initData) {
                if (obj.hasOwnProperty(prop)) {
                  if (prop in initData) {
                    this.initData[prop] = variation.initData[prop];
                  }
                }
              }
            }
            // Apply variations to initData.

            // Straight text swaps.
            this.variationReplacements.forEach(([changeFrom, changeTo]) => {
              this.initData.publicTitle = this.initData.publicTitle.replaceAll(
                changeFrom,
                changeTo
              );
              this.initData.tweet = this.initData.tweet.replaceAll(
                changeFrom,
                changeTo
              );
              this.initData.whatsappText =
                this.initData.whatsappText.replaceAll(changeFrom, changeTo);
            });

            // Swap HTML for HTML output
            d.replacements
              .map((pair) => pair.map(convertTextToHtml))
              .forEach(([changeFrom, changeTo]) => {
                [
                  "introHTML",
                  "preOptinHTML",
                  "smallprintHTML",
                  "shareAskHTML",
                  "finalHTML",
                ].forEach((prop) => {
                  this.initData[prop] = this.initData[prop].replaceAll(
                    changeFrom,
                    changeTo
                  );
                  CiviCRMInlay.debug({
                    changeFrom,
                    changeTo,
                    subject: initData[prop],
                  });
                });
              });
            CiviCRMInlay.debug("petition after replacements", d);

            // Swap count for variation count.
            this.initData.count =
              this.inlay.initData.variationCounts[this.variation] || 0;
          } catch (e) {
            console.error(e);
            alert(
              "Sorry! This form is not configured properly. Please let us know so we can fix it."
            );
          }
        },

        animation(t) {
          const justStarted = !this.ometer.start;
          if (justStarted) {
            CiviCRMInlay.debug(
              "animation: no ometer.start, so setting to given:",
              t
            );
            this.ometer.start = t;
          }
          // Allow 1s (1000ms) for the animation.
          this.ometer.step = Math.min(1, (t - this.ometer.start) / 1000);
          this.updateAnimatedModels();

          // Do we need to continue animating?
          if (this.ometer.step < 0) {
            CiviCRMInlay.debug("Darned step is <0", {
              step: this.ometer.step,
              t,
              ometerStart: this.ometer.start,
              tMinus: t - this.ometer.start,
            });
            return;
          }
          if (this.ometer.step < 1) {
            if (justStarted) {
              CiviCRMInlay.debug("continuing animation", this);
            }
            window.requestAnimationFrame(this.animation.bind(this));
            // Next time, start from here.
            this.ometer.animatedCountFrom = this.initData.count;
          } else {
            CiviCRMInlay.debug("stopping animation", this);
          }
        },
        updateAnimatedModels() {
          // use square
          const fract = this.ometer.step * this.ometer.step,
            range = this.initData.count - this.ometer.animatedCountFrom;
          // animatedCount is the shown number.
          this.ometer.animatedCount =
            Math.ceil(fract * range) + this.ometer.animatedCountFrom;
          if (this.target == 0) return;
          // Set width of target bar in chart
          const percentOfTarget = Math.min(
            (this.ometer.animatedCount / this.target) * 100,
            100
          );
          this.ometer.style.width = `${percentOfTarget}%`;
        },
        clickedSocial(e) {
          e.stopPropagation();
          if (this.initData.followupUx === 'shareThanks') this.changeStage('final');
          const inlaypetitionshared = new CustomEvent('inlaypetitionshared',
            { detail: {
                inlay,
                socialNetworkName: e.detail,
                publicTitle: inlay.initData.publicTitle,
                publicID: inlay.publicID,
                optIn: inlay.userData.optin,
                uxMode: inlay.initData.uxMode,
            }, bubbles: true }
          );
          this.$root.dispatchEvent(inlaypetitionshared);
        },
        skippedSocial(e) {
          this.changeStage('final')
          const inlaypetitionskippedsocial = new CustomEvent('inlaypetitionskippedsocial',
            { detail: {
                inlay,
                publicTitle: inlay.initData.publicTitle,
                publicID: inlay.publicID,
                optIn: inlay.userData.optin,
                uxMode: inlay.initData.uxMode,
            }, bubbles: true }
          );
          this.$root.dispatchEvent(inlaypetitionskippedsocial);
        },

        changeStage(newStage) {
          this.stage = newStage;
          // remove PII from URL after first page.
          window.location.hash = "#"; // Cannot set it empty; causes page reload!
          const headerOffset = 100; // Allow 100px for headers
          window.scrollTo({
            top:
              this.$refs.app.getBoundingClientRect().top -
              document.body.getBoundingClientRect().top -
              headerOffset,
            behavior: "smooth",
          });
        },

        removePrefillData() {
          this.cs = this.ctID = null;
          // remove PII from URL
          history.replaceState(
            "",
            document.title,
            window.location.href.replace(/#.*$/, "")
          );
          this.userData = {
            first_name: "",
            last_name: "",
            email: "",
            postcode: "",
          };
        },

        async submitForm() {
          // Form is valid according to browser.
          this.submissionRunning = true;
          const d = {
            need: "submitData",
            first_name: this.userData.first_name,
            last_name: this.userData.last_name,
            email: this.userData.email,
            location: window.location.href,
            optin: this.userData.optin,
            postcode: this.userData.postcode || "",
          };

          if (this.initData.allowVariations) {
            d.variation = this.variation;
          }

          if (this.cs && this.cid) {
            d.cs = this.cs;
            d.ctID = this.ctID;
          }

          const progress = this.$refs.progress;
          // Allow 5s to get 20% through in first submit.
          progress.startTimer(5, 20, { reset: 1 });
          try {
            let r = await this.request({ method: "post", body: d }).then(
              (r) => {
                if (r.token) {
                  d.token = r.token;
                  // Allow 2s for our wait, taking us to 70% linearly.
                  progress.startTimer(2, 70, { easing: false });
                  // Force 2s wait for the token to become valid
                  return new Promise((resolve) =>
                    window.setTimeout(resolve, 2000)
                  );
                } else {
                  console.warn("unexpected resonse", r);
                  throw r.error || "Unknown error";
                }
              }
            );
            // Finally allow 7s for the final submission to 100%
            progress.startTimer(7, 100);
            r = await this.request({ method: "post", body: d });
            if (r.error) {
              throw r.error;
            }
            // Increment totals.
            this.initData.count += 1;
            this.lastSigner = "Last signed by You, just now!";
            // Show full animation again.
            this.ometer.animatedCountFrom = 0;
            this.startAnimation();
            await progress.cancelTimer();
            if (this.initData.followupUx === "shareThanks") {
              this.changeStage("social");
            } else {
              this.changeStage("final");
            }
            const signedEvent = new CustomEvent('inlaypetitionsigned', {
              detail: {
                inlay,
                publicTitle: inlay.initData.publicTitle,
                publicID: inlay.publicID,
                optIn: inlay.userData.optin,
                uxMode: inlay.initData.uxMode,
              }, bubbles: true });
            console.log("inlaypetitionsigned", signedEvent);
            this.$root.dispatchEvent(signedEvent);
          } catch (e) {
            if (typeof e === "string") {
              alert(e);
            } else {
              alert("Unexpected error");
            }
            this.submissionRunning = false;
            progress.cancelTimer();
          }
        },
      });

      // We need to put the inlay object somewhere Alpine can find it.
      CiviCRMInlay.alpineData = CiviCRMInlay.alpineData ?? {};
      CiviCRMInlay.alpineData[inlay.publicID] = inlay;

      // The HTML is fetched from app.html by the bundler; it's nicer to edit as a .html file.
      const headingTag = inlay.initData.isPageTitle ? "h1" : "h2";
      inlay.script.insertAdjacentHTML(
        "afterend",
        `<div class="inlay-petition" x-data="CiviCRMInlay.alpineData['${inlay.publicID}']" x-ref="app" >
          <${headingTag} x-text="initData.publicTitle"></${headingTag}>` +
          inlay.initData.layoutHTML + //window.inlayPetitionHtml +
          `</div>`
      );

      // Do we have custom CSS?
      if (
        inlay.initData.css &&
        !document.getElementById("inlay-petition-css-" + inlay.publicID)
      ) {
        let css = document.createElement("style");
        css.id = "inlay-petition-css-" + inlay.publicID;
        css.innerHTML = inlay.initData.css;
        document.head.appendChild(css);
      }
    };

    // Inject some static CSS
    if (!document.getElementById("inlay-petition-css")) {
      let css = document.createElement("style");
      css.id = "inlay-petition-css";
      css.innerHTML = `
      .inlay-petition {
        background-color: var(--inlay-petition-background, #fdfdfd);
        padding: var(--inlay-petition-padding, 2vw);
        margin-bottom: 2em;
      }
      .inlay-petition h2:first-child { margin-top: 0; }
      .inlay-petition img { max-width: 100%;}
      .ipet-optins .option { position: relative; padding-left: 1.5em; }
      .ipet-optins input { position: absolute; top: 0.1em; margin-left: -1.5em; }
      .ipet-input-wrapper label { display: block; }
      .ipet-input-wrapper input { width: 100%; box-sizing: border-box; }
      .ipet-preoptin { margin-top: 1em; }
      .ipet-smallprint { margin: 1em 0; font-size: 0.875em; }
      .ipet-submit { margin-top: 1em; }
      .ipet-submit button { padding: 1em 3em; }
      .ipetometer { display:grid; grid-template-columns: auto 1fr auto; gap: 0 1ch; font-size: 0.875em; }
      .ipetometer__domain { grid-column: 1/4; background-color: var(--inlay-petition-bar-domain, #f0f0f0); height: 1em; }
      .ipetometer__bar { background-color: var(--inlay-petition-bar, #13b0d4); height: 1em; }
      .ipetometer__last-signer { font-style: italic; grid-column: 1 / 4; }
      .ipet-2-cols { display: flex; gap: 1em; flex-wrap: wrap; }
      .ipet-2-cols .ipet-input-wrapper { flex: 1 0 10ch; }
      .ipet-layout-two-column-a-cols { display: flex; gap: 2em; flex-wrap: wrap;}
      .ipet-layout-col-1 { flex: 4 0 200px; }
      .ipet-layout-col-2 { flex: 1 0 200px; }
      .inlay-petition inlay-progress { margin-top: 0.5rem; }
      .inlay-petition progress,
      .inlay-petition progress::-webkit-progress-bar {
        -webkit-appearance: none;
        -moz-appearance: none;
         appearance: none;
         border: none;
         height: 6px;
         background-color: var(--inlay-petition-bar-domain, #f0f0f0);
      }
      .inlay-petition progress::-webkit-progress-value,
      .inlay-petition progress::-moz-progress-bar {
         background-color: var(--inlay-petition-bar, #13b0d4);
      }
      `;
      document.head.appendChild(css);
    }
  }
})();
