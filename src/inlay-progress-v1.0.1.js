// Create progress Web Component
// v1.0.1: set display:block on custom element, so margin can work.
if (!customElements.get("inlay-progress")) {
  customElements.define(
    "inlay-progress",
    class extends HTMLElement {
      static n = 0;
      doneBefore = 0;
      percentDoneAtEndOfJob = 100;
      expectedTime = null;
      percent = 0;
      start = null;
      running = false; /* if running, it's shown, otherwise not. */
      stalled = false;
      easing = true;
      completing = false;
      label = null;
      progressElement = null;

      constructor() {
        super();
        this.constructor.n++;
      }

      connectedCallback() {
        this.progressElement = document.createElement("progress");
        let id = "inlay-progress-" + this.constructor.n;
        this.progressElement.id = id;
        this.progressElement.max = 100;
        this.progressElement.value = 0;
        this.appendChild(this.progressElement);

        this.label = document.createElement("label");
        this.label.classList.add("progress-label");
        this.label.setAttribute("for", id);
        this.appendChild(this.label);

        // Hard-coded style. Hmmm.
        this.style.opacity = 0;
        this.style.transition = "0.3s opacity";
        this.style.pointerEvents = "none";
        this.style.display = "block";
        this.progressElement.style.display = "block";
        this.progressElement.style.width = "100%";
      }

      disconnectedCallback() {
        this.label.remove();
      }

      startTimer(expectedTimeForNewJob, percentDoneAtEndOfNewJob, opts) {
        this.expectedTime = expectedTimeForNewJob * 1000;
        this.percentDoneAtEndOfJob = percentDoneAtEndOfNewJob;
        opts = Object.assign({ reset: false, easing: true }, opts || {});
        // console.log('startTimer', {when: (new Date()).toISOString().substr(11, 8), expectedTime, percentDoneAtEndOfJob, opts});

        if (opts.reset) {
          this.doneBefore = 0;
          this.percent = 0;
          this.start = null;
          this.running = false; // gets changed instantly below.
          this.stalled = false;
          this.completing = false;
        } else {
          // Adding a job.
          this.doneBefore = this.percent;
          this.start = null;
        }
        this.style.opacity = 1;
        this.style.pointerEvents = "";
        this.label.textContent = opts.task || "";
        // Always default to using easing.
        this.easing = opts.easing;
        if (!this.running || this.stalled) {
          // Start animation.
          this.running = true;
          this.stalled = false;
          window.requestAnimationFrame(this.animateTimer.bind(this));
        }
      }
      cancelTimer() {
        this.start = null;
        this.running = false;
        this.completing = false;
        this.label.textContent = "";
        this.style.opacity = 0;
        this.style.pointerEvents = "none";
      }
      completeTimer() {
        this.completing = true;
        this.label.textContent = "";
        this.startTimer(0.3, 100);
      }
      animateTimer(t) {
        if (!this.start) {
          this.start = t;
        }
        const linear = Math.min(1, (t - this.start) / this.expectedTime);
        var fraction = linear;
        if (this.easing) {
          // This is more performant than Math.pow((1-fraction), 3)
          fraction = 1 - (1 - fraction) * (1 - fraction) * (1 - fraction);
        }
        this.percent =
          this.doneBefore +
          fraction * (this.percentDoneAtEndOfJob - this.doneBefore);

        this.progressElement.value = this.percent;

        if (this.running) {
          if (linear < 1) {
            // We still have stuff to animate.
            window.requestAnimationFrame(this.animateTimer.bind(this));
          } else if (this.completing) {
            // This is called when completeTimer() has completed.
            this.completing = false;
            this.cancelTimer();
          } else {
            this.stalled = true;
          }
        }
      }
    }
  );
}
