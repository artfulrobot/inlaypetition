const s = document.currentScript;
document.addEventListener("DOMContentLoaded", () => {
  // Alpine might have loaded before we get to run, or we might need to load it.
  function loadAlpineAndBoot() {
    if (!window.Alpine) {
      const alpScr = document.createElement("script");
      alpScr.addEventListener("load", boot);
      alpScr.src = "https://unpkg.com/alpinejs";
      document.body.append(alpScr);
      console.log("should be loading Alpine", alpScr);
    } else {
      console.log("window.Alpine exists", window.Alpine);
      boot();
    }
  }

  // Now we have Alpine, we can boot our app.
  function boot() {
    console.log("Alpine has loaded");

    // We can supply a function that will return data for a component
    // This one would return the data for an inlay.
    // Using a function gives you the possibility to return other stuff too.
    Alpine.data("getInlayData", arg1 => {
      console.log("getInlayData running", arg1);
      return window.CiviCRMInlay.inlays[arg1];
    });

    s.insertAdjacentHTML(
      "afterend",
      `
  <div id=root x-data="getInlayData('aabbccddee')" >
    <p>This is generated in js for <span x-text="name"></span></p>
    <p x-show="ok">ok</p>
    <p x-show="!ok">not ok</p>
  </div>
    `
    );
  }

  window.CiviCRMInlay = { inlays: { aabbccddee: { name: "Wilma", ok: true } } };
  loadAlpineAndBoot();

  // r.setAttribute(
  //   "x-data",
  //   JSON.stringify({
  //     name: "Nettie",
  //     ok: false
  //   })
  // );
});
