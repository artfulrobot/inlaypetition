// Create inlay-socials web component
//
// <inlay-socials
//    socials="tweet email facebook"
//    icons=1
//    with-style
// ></inlay-socials>
//
// The 'socials' attribute is a space separated list of the following possibilities.
//
// * twitter. Uses tweet†
// * facebook. Url only.
// * whatsapp. Uses whatsapp-text†
// * linkedin. Uses linkedin-text†
// * email. Uses email-subject† URL is added to body.
// * bluesky. Uses bluesky-text†
// * threads. Uses threads-text†
// * mastodon. Uses mastodon-text†
//
// † if the more specific attribute is not set, use share-text.
//
// If using with-style the following CSS variables are used:
//
// * --inlay-socials-radius
// * --inlay-socials-color
// * --inlay-socials-color-hover
// * --inlay-socials-color-visited
// * --inlay-socials-background (this is background-color)
// * --inlay-socials-background-hover
//
// v1.1.1: Emit CustomEvent with {detail: socialNetworkName}
// v1.1.0: Add Threads, Bluesky, Clipboard and Mastodon. Fixup default style
// v1.0.2: updated Xitter logo
if (!customElements.get("inlay-socials")) {
  customElements.define(
    "inlay-socials",
    class extends HTMLElement {
      static observedAttributes = [
        "socials",
        "tweet",
        "whatsapp-text",
        "email-subject",
        "linkedin-text",
      ];
      static n = 0;
      static icons = {
        twitter: `<svg viewBox="0 0 1200 1227" fill="none" xmlns="http://www.w3.org/2000/svg" width="18" height="18"><title>X</title><path d="M714 519 1161 0h-106L667 451 357 0H0l468 682L0 1226h106l409-476 328 476h357L714 519ZM569 688l-47-68L144 80h163l304 436 48 68 396 566H892L569 688Z" fill="currentColor"/></svg>`,
        facebook: `<svg viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg" width="18" height="18"><title>Facebook</title><path d="M0 7.5a7.5 7.5 0 118 7.484V9h2V8H8V6.5A1.5 1.5 0 019.5 5h.5V4h-.5A2.5 2.5 0 007 6.5V8H5v1h2v5.984A7.5 7.5 0 010 7.5z" fill="currentColor"></path></svg>`,
        whatsapp: `<svg viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg" width="18" height="18"><title>WhatsApp</title><path d="M5 4.768a.5.5 0 01.761.34l.14.836a.5.5 0 01-.216.499l-.501.334A4.501 4.501 0 015 5.5v-.732zM9.5 10a4.5 4.5 0 01-1.277-.184l.334-.5a.5.5 0 01.499-.217l.836.14a.5.5 0 01.34.761H9.5z" fill="currentColor"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0 7.5a7.5 7.5 0 113.253 6.182l-2.53 1.265a.5.5 0 01-.67-.67l1.265-2.53A7.467 7.467 0 010 7.5zm4.23-3.42l.206-.138a1.5 1.5 0 012.311 1.001l.14.837a1.5 1.5 0 01-.648 1.495l-.658.438A4.522 4.522 0 007.286 9.42l.44-.658a1.5 1.5 0 011.494-.648l.837.14a1.5 1.5 0 011.001 2.311l-.138.207a.5.5 0 01-.42.229h-1A5.5 5.5 0 014 5.5v-1a.5.5 0 01.23-.42z" fill="currentColor"></path></svg>`,
        linkedin: `<svg viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg" width="18" height="18"><title>LinkedIn</title><path fill-rule="evenodd" clip-rule="evenodd" d="M0 1.5A1.5 1.5 0 011.5 0h12A1.5 1.5 0 0115 1.5v12a1.5 1.5 0 01-1.5 1.5h-12A1.5 1.5 0 010 13.5v-12zM5 5H4V4h1v1zm-1 6V6h1v5H4zm4.5-4A1.5 1.5 0 007 8.5V11H6V6h1v.5a2.5 2.5 0 014 2V11h-1V8.5A1.5 1.5 0 008.5 7z" fill="currentColor"></path></svg>`,
        email: `<svg viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg" width="18" height="18"><title>Email</title><path d="M.5 2.5A1.5 1.5 0 012 1h12a1.5 1.5 0 011.5 1.5v1.208L8 7.926.5 3.708V2.5z" fill="currentColor"></path><path d="M.5 4.855V12.5A1.5 1.5 0 002 14h12a1.5 1.5 0 001.5-1.5V4.855L8 9.074.5 4.854z" fill="currentColor"></path></svg>`,
        threads: `<svg viewBox="0 0 15 15" width="18" height="18" xmlns="http://www.w3.org/2000/svg"><title>Threads></title><path fill="currentColor" d="M11.09 6.95a5.2 5.2 0 0 0-.2-.09c-.11-2.13-1.28-3.35-3.24-3.36h-.02c-1.17 0-2.15.5-2.75 1.4l1.08.74a1.9 1.9 0 0 1 1.69-.82c.64 0 1.13.2 1.44.56.23.26.38.63.46 1.1a8.24 8.24 0 0 0-1.85-.1c-1.86.11-3.06 1.2-2.98 2.7.04.77.42 1.43 1.08 1.86.55.36 1.26.54 2 .5a2.95 2.95 0 0 0 2.26-1.1c.4-.52.66-1.19.77-2.03.47.28.81.65 1 1.1a2.7 2.7 0 0 1-.67 2.99c-.88.88-1.94 1.26-3.55 1.28-1.78-.02-3.13-.59-4-1.7-.83-1.05-1.25-2.55-1.27-4.48.02-1.93.44-3.43 1.27-4.48.87-1.11 2.22-1.68 4-1.7 1.8.02 3.17.6 4.08 1.71.44.55.78 1.24 1 2.04l1.26-.33c-.27-1-.69-1.85-1.26-2.55C11.52.75 9.8.02 7.62 0H7.6C5.4.02 3.73.75 2.59 2.2c-1 1.28-1.52 3.06-1.54 5.3.02 2.24.54 4.02 1.54 5.3 1.14 1.45 2.82 2.18 5.02 2.2 1.96-.01 3.33-.52 4.46-1.66a4.06 4.06 0 0 0 .95-4.47 3.83 3.83 0 0 0-1.93-1.92Zm-3.37 3.17c-.81.04-1.66-.32-1.7-1.1-.03-.59.41-1.24 1.75-1.31a7.85 7.85 0 0 1 1.81.12c-.15 1.93-1.06 2.24-1.86 2.29Z" style="stroke-width:.078125"/></svg>`,
        bluesky: `<svg viewBox="0 0 600 530" width=18 height=18 version="1.1" xmlns="http://www.w3.org/2000/svg"><title>Bluesky</title><path d="m135.72 44.03c66.496 49.921 138.02 151.14 164.28 205.46 26.262-54.316 97.782-155.54 164.28-205.46 47.98-36.021 125.72-63.892 125.72 24.795 0 17.712-10.155 148.79-16.111 170.07-20.703 73.984-96.144 92.854-163.25 81.433 117.3 19.964 147.14 86.092 82.697 152.22-122.39 125.59-175.91-31.511-189.63-71.766-2.514-7.3797-3.6904-10.832-3.7077-7.8964-0.0174-2.9357-1.1937 0.51669-3.7077 7.8964-13.714 40.255-67.233 197.36-189.63 71.766-64.444-66.128-34.605-132.26 82.697-152.22-67.108 11.421-142.55-7.4491-163.25-81.433-5.9562-21.282-16.111-152.36-16.111-170.07 0-88.687 77.742-60.816 125.72-24.795z" fill="currentColor"/></svg>`,
        mastodon: `<svg width="18" height="18" viewBox="0 0 480 480" xmlns="http://www.w3.org/2000/svg"><title>Mastodon</title><path fill="currentColor" d="M239.3-.23c-61.43.5-120.52 7.15-154.95 22.96 0 0-68.3 30.55-68.3 134.78 0 23.87-.46 52.4.3 82.67C18.83 342.1 35.03 442.56 129.27 467.5c43.46 11.5 80.76 13.9 110.81 12.26 54.49-3.02 85.08-19.45 85.08-19.45l-1.8-39.53s-38.94 12.27-82.67 10.78c-43.32-1.49-89.06-4.67-96.07-57.87a109.1 109.1 0 0 1-.97-14.92s42.53 10.4 96.43 12.87c32.96 1.51 63.87-1.93 95.26-5.68 60.2-7.18 112.62-44.28 119.2-78.17 10.38-53.39 9.53-130.29 9.53-130.29 0-104.23-68.29-134.78-68.29-134.78C361.35 6.92 302.23.27 240.8-.23ZM169.76 81.2c25.6 0 44.96 9.83 57.77 29.5L240 131.59l12.46-20.88c12.8-19.67 32.18-29.5 57.77-29.5 22.1 0 39.93 7.77 53.53 22.93 13.2 15.17 19.76 35.67 19.76 61.46v126.2h-50V169.3c0-25.81-10.87-38.92-32.6-38.92-24.03 0-36.07 15.54-36.07 46.29v67.05h-49.7v-67.05c0-30.75-12.05-46.3-36.07-46.3-21.73 0-32.6 13.12-32.6 38.94v122.5h-50V165.6c0-25.8 6.57-46.3 19.76-61.46 13.6-15.16 31.42-22.93 53.53-22.93z" style="stroke-width:1"/></svg>`,
        copy: `<svg width="18" height="18" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Copy to clipboard</title><path style="fill-rule:evenodd;stroke-width:1.72403;-inkscape-stroke:none" d="M10.9 0A4.8 4.8 0 0 0 6 4.8v10.4a5 5 0 0 0 5 4.8h6.3c2.6 0 4.8-2.2 4.8-4.8V4.8A5 5 0 0 0 17.2 0h-6.3zM5 5C3.8 6 3 7.5 3 9.2v8.3c0 3 2.5 5.5 5.5 5.5h4c1.6 0 3-.6 4-1.7H9.8A4.8 4.8 0 0 1 5 16.5V5zm8 0h2v4h4v2h-4v4h-2v-4H9V9h4V5z"/></svg>`,
      };
      static labels = {
        twitter: "X",
        facebook: "Facebook",
        whatsapp: "WhatsApp",
        linkedin: "LinkedIn",
        email: "Email",
        threads: "Threads",
        bluesky: "Bluesky",
        mastodon: "Mastodon",
        copy: "Copy link",
      };

      debounce = false;

      constructor() {
        super();
        this.constructor.n++;
      }

      connectedCallback() {
        if (
          this.hasAttribute("with-style") &&
          !document.getElementById("inlay-socials-default-style")
        ) {
          const css = document.createElement("style");
          css.innerHTML = `
ul.inlay-socials {
  list-style: none;
  padding: 0;
  margin-left: 0;
  margin-right: 0;
  display: flex;
  flex-wrap: wrap;
  gap: 1rem;
  list-style: none;
}
ul.inlay-socials li {
  margin: 0;
  padding: 0;
  flex: 1 0 auto;
  list-style: none;
}
ul.inlay-socials a {
  border-radius: var(--inlay-socials-radius, 4px);
  display: block;
  box-sizing: border-box;
  height: 100%;
  text-align: center;
  padding: 1em 2rem;
  line-height: 1;
  background-color: var(--inlay-socials-background, #e8e8e8);
  color: var(--inlay-socials-color, #222);
  text-decoration: none;
}
ul.inlay-socials  a:hover, ul.inlay-socials  a:visited, ul.inlay-socials a:active {
  text-decoration: none;
  color: var(--inlay-socials-color-visited, #666);
}
ul.inlay-socials a:hover, ul.inlay-socials a:active {
  background-color: var(--inlay-socials-background-hover, #e0e0e0);
  color: var(--inlay-socials-color-hover, #111);
}
svg {
  vertical-align: middle;
  display: inline-block;
  margin-right: 1em;
}`;
          document.head.appendChild(css);
        }
        this.render();
      }

      render() {
        if (!this.hasAttribute("socials")) {
          this.innerHTML = "";
          return;
        }
        if (this.debounce) {
          clearTimeout(this.debounce);
        }
        this.debounce = setTimeout(() => {
          // We strip off the hash since this can contain a checksum
          const url = this.getUrl(),
            urlEncoded = encodeURIComponent(url),
            ul = document.createElement("ul");
          ul.classList.add("inlay-socials");

          const useFn = (link, fnName, e) => {
            link.href = "#";
            link.addEventListener("click", (e) => {
              e.preventDefault();
              e.stopPropagation();
              this[fnName](e);
            });
          };

          (this.getAttribute("socials").split(" ") || []).forEach((social) => {
            const li = document.createElement("li"),
              link = document.createElement("a");
            link.target = "_blank";
            link.classList.add("inlay-socials--" + social);
            link.addEventListener("click", this.handleClick.bind(this));
            if (social === "twitter") {
              let tweet = this.getShareText("tweet");
              link.href = `https://twitter.com/intent/tweet/?text=${tweet}&url=${urlEncoded}`;
            } else if (social === "facebook") {
              link.href = `https://facebook.com/sharer/sharer.php?u=${urlEncoded}`;
            } else if (social === "whatsapp") {
              let whatsappText = this.getShareText("whatsapp-text", true);
              link.href = `whatsapp://send?text=${whatsappText}`;
            } else if (social === "linkedin") {
              let title = this.getShareText("linkedin-text");
              link.href = `https://www.linkedin.com/shareArticle?mini=true&url=${urlEncoded}&title=${title}&summary=${title}&source=${urlEncoded}`;
            } else if (social === "bluesky") {
              let text = this.getShareText("bluesky-text", true);
              link.href = `https://bsky.app/intent/compose?text=${text}`;
            } else if (social === "threads") {
              let text = this.getShareText("threads-text", true);
              link.href = `https://threads.net/intent/post?text=${text}`;
            } else if (social === "mastodon") {
              useFn(link, "shareToMastodon");
              // Defer.
            } else if (social === "copy") {
              useFn(link, "copyToClipboard");
            } else if (social === "email") {
              let subject = this.getShareText("email-subject");
              link.href = `mailto:?subject=${subject}&body=${urlEncoded}`;
            }
            if (this.getAttribute("icons")) {
              link.innerHTML = this.constructor.icons[social] || "";
            }
            link.append(this.constructor.labels[social]);
            li.appendChild(link);
            ul.appendChild(li);
          });
          this.innerHTML = "";
          this.appendChild(ul);
        }, 25);
      }

      handleClick(sn) {
        const event = new CustomEvent("clicked", { detail: sn });
        this.dispatchEvent(event);
      }

      shareToMastodon() {
        let instance = (
          prompt(
            "Share to Mastodon:\nWhat’s your instance’s domain\ne.g. kind.social or mastodon.social\n"
          ) || ""
        ).toLowerCase();
        if (!instance) return;
        instance = instance.replace(/^https?:[/]{2}/, "");
        let m = instance.match(/^@?\w+@((?:[a-z0-9]+\.)+[a-z]+)$/);
        if (m) {
          instance = m[1];
        }
        if (!instance.match(/^(?:[a-z0-9]+\.)+[a-z]+$/)) {
          alert("Sorry, that didn’t look right.");
          return;
        }
        let text = this.getShareText("mastodon-text", true);
        window.open(`https://${instance}/share?text=${text}`);
      }

      copyToClipboard(e) {
        const input = document.createElement("input");
        const url = this.getUrl();
        input.value = url;
        document.body.appendChild(input);
        input.select();
        input.setSelectionRange(0, url.length); /* For mobile devices */
        document.execCommand("copy");
        document.body.removeChild(input);
        const el = e.target,
          orig = el.innerHTML;
        el.textContent = "✔ Link copied";
        setTimeout(() => {
          el.innerHTML = orig;
        }, 2000);
      }

      getUrl() {
        return window.location.href.replace(/#.*/, "");
      }

      getShareText(preferred, ensureUrl) {
        let v = this.getAttribute(preferred) || this.getAttribute("share-text");
        if (ensureUrl && !v.match(/https:[/]{2}/)) {
          const url = this.getUrl();
          v = v ? `${v}\n\n${url}` : url;
        }
        return encodeURIComponent(v);
      }

      attributeChangedCallback(name, oldValue, newValue) {
        this.render();
      }

      disconnectedCallback() {
        // this.innerHTML = '';
      }
    }
  );
}
