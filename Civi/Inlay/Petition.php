<?php

namespace Civi\Inlay;

use Civi\Inlay\Type as InlayType;
use Civi\Api4\OptionValue;
use Civi;
use Civi\Api4\Activity;
use Civi\Api4\Address;
use Civi\Api4\Contact;
use Civi\Api4\PostcodesIO;
use Civi\Core\Event\GenericHookEvent;
use CRM_Inlaypetition_ExtensionUtil as E;
use CRM_Queue_Task;
use CRM_Queue_Service;
use CRM_Queue_Queue;

class Petition extends InlayType {

  const EVENT_ALTER_PIPELINE = 'civi.inlaypetition.alterpipeline';

  const VALID_UK_POSTCODE_REGEX = '/^(GIR 0AA)|((([A-Z][0-9][0-9]?)|(([A-Z][A-HJ-Y][0-9][0-9]?)|(([A-Z][0-9][A-Z])|([A-Z][A-HJ-Y][0-9]?[A-Z])))) [0-9][A-Z]{2})$/';

  const MAX_COUNT_AGE_SECONDS = 30;

  /**
   * Access this via getActivityTypeID()
   */
  public static $petitionActivityType = NULL;

  /**
   * Access this via getSignerOptInFieldID()
   */
  public static $signerOptInFieldID = NULL;

  public static $typeName = 'Petition/signup';

  /**
   * Cache so that when processing a set of queued signups we don't have to
   * load the Inlay instance for each time.
   *
   * Keyed by instance ID.
   */
  public static $instanceCache = [];

  public static $defaultConfig = [
    // Nb. the inlay's name is used for the petition activity subject.

    // Form
    'publicTitle'       => '',
    'isPageTitle'       => FALSE,
    'target'            => NULL,
    'uxMode'            => 'petition', /* petition|signup */
    'uiLayout'          => 'layout-single-column', /* layout-single-column | layout-two-column-a | custom */
    // Page 1
    'introHTML'         => '',
    'askPostcode'       => 'no', /* no|uk|other */
    'emailConfirmation' => 'none', /* none|repeat */
    'askPhone'          => 'no',
    'preOptinHTML'      => '', /* radios|none|checkbox */
    'optinMode'         => 'radios',
    'optinYesText'      => 'I would like to receive further information from this organisation about its campaigns and activities, in the future',
    'optinNoText'       => 'I’m happy as I am, thanks',
    'smallprintHTML'    => NULL,
    'submitButtonText'  => 'Sign', /* share|thanks */
    'followupUx'        => 'shareThanks', /* shareThanks|thanksShare|thanks */
    'imageAlt'          => '',
    'css'               => '',

    // (Social Media ask)
    'shareAskHTML'      => '<h2>Thanks!</h2><p>Can you share this?</p>',
    'socials'           => ['twitter', 'facebook', 'email', 'whatsapp'],
    'shareText'         => '',
    'blueskyText'       => '',
    'threadsText'       => '',
    'mastodonText'      => '',
    'linkedInText'      => '',
    'shareText'         => '',
    'tweet'             => '',
    'whatsappText'      => '',
    'shareEmailSubject' => '',

    // Final/thanks
    'finalHTML'             => '<h2>Thank you</h2><p>Are you able to make a donation to our work?</p>',
    'thanksMsgTplID'        => NULL,
    'thanksEmailCustomHTML' => '',

    // Internals
    'mailingGroup'          => NULL,
    'allowVariations'       => FALSE,
    'useQueue'              => TRUE,
  ];

  /**
   * Note: because of the way CRM.url works, you MUST put a ? before the #
   *
   * @var string
   */
  public static $editURLTemplate = 'civicrm/a?#/inlay/petition/{id}';

  /**
   * Generates data to be served with the Javascript application code bundle.
   *
   * @return array
   */
  public function getInitData(): array {

    $data = [];
    foreach ([
      // UX
      'uxMode',
      // Top bit
      'publicTitle', 'isPageTitle', 'target',
      // First view
      'introHTML', 'askPostcode', 'emailConfirmation', 'preOptinHTML', 'optinMode', 'optinYesText', 'optinNoText', 'smallprintHTML', 'submitButtonText', 'imageAlt',
      // Social share ask
      'shareAskHTML', 'tweet', 'whatsappText', 'shareEmailSubject', 'linkedinText',
      'shareText', 'blueskyText', 'threadsText', 'mastodonText', 'linkedInText',
      // Final thanks.
      'finalHTML',
      'allowVariations',
      'followupUx',
    ] as $_) {
      $data[$_] = $this->config[$_] ?? '';
    }

    if ($this->config['uiLayout'] === 'custom') {
      $tplPath = Civi::paths()->getPath("[civicrm.files]/inlaypetition/layout-" . $this->getID() . '.html');
    }
    else {
      $tplPath = E::path("src/{$this->config['uiLayout']}.html");
    }
    $data['layoutHTML'] = file_exists($tplPath) ? file_get_contents($tplPath) : '<p class=error>Misconfigured form: layout missing.</p>';

    // CSS
    $data['css'] = '';
    if ($this->config['css']) {
      $css = trim($this->config['css']);
      if (strpos($css, '<') === FALSE) {
        $data['css'] = $css;
      }
    }

    // Social share data.
    $data['socials'] = implode(' ', $this->config['socials']);

    $data['imageUrl'] = Asset::singleton()->getAssetUrl('inlaypetition_' . $this->getID());

    // Custom output per uxMode
    switch ($this->config['uxMode']) {
      case 'petition':
        $data['init'] = 'inlayPetitionInit';
        // Always get up-to-date count when rebuilding.
        $data = array_merge($data, $this->getCounts(0));
        break;

      case 'signup':
        $data['init'] = 'inlayPetitionInit';
        break;

      default:
        // Should never happen.
        throw new \InvalidArgumentException("Bad configuration on Petition/signup Inlay " . $this->getID());
    }

    return $data;
  }

  /**
   * Find our activity type
   */
  public static function getActivityTypeID() {
    if (!static::$petitionActivityType) {
      // Look it up.
      static::$petitionActivityType = OptionValue::get(FALSE)
        ->addSelect('value')
        ->addWhere('option_group_id:name', '=', 'activity_type')
        ->addWhere('name', '=', 'inlay_petition')
        ->execute()->first()['value'] ?? 0;
    }
    return static::$petitionActivityType;
  }

  /**
   * Find our activity type
   */
  public static function getSignerOptInFieldID() {
    if (!static::$signerOptInFieldID) {
      // Look it up.
      static::$signerOptInFieldID = \Civi\Api4\CustomField::get(FALSE)
        ->addWhere('custom_group_id:name', '=', 'inlaypetition_signer')
        ->addWhere('name', '=', 'inlaypetition_signer_optin')
        ->execute()->first()['id'];
    }
    return static::$signerOptInFieldID;
  }

  public function postcodesioExtensionInstalled(): bool {
    if (!isset(\Civi::$statics['usePostcodesIO'])) {
      \Civi::$statics['usePostcodesIO'] = 'installed' === (\CRM_Extension_System::singleton()->getManager()->getStatuses()['postcodesio'] ?? NULL);
    }
    return \Civi::$statics['usePostcodesIO'];
  }

  /**
   * Validate a UK postcode using a regex or postcodes.io
   */
  public function validateUKPostcode(string $rawPostcode, array &$valid): bool {

    if (!$this->postcodesioExtensionInstalled()) {
      return $this->validateUKPostcodeByRegex($rawPostcode, $valid);
    }

    // Use Artful Robot’s postcodesio extension.
    try {
      $result = PostcodesIO::refresh(FALSE)->setPostcode($rawPostcode)->execute();
    }
    catch (\Exception $e) {
      // Don’t make a hoo har if the api is down, just use our regex.
      Civi::log()->error("Failed calling PostcodesIO.refresh Will use regex instead. "
        . $e->getMessage(), ['postcode' => $rawPostcode, 'exception' => $e]);

      return $this->validateUKPostcodeByRegex($rawPostcode, $valid);
    }
    $correctPostcode = $result['record']['json']['postcode'] ?? '';
    if (empty($correctPostcode)) {
      return FALSE;
    }

    // Ensure we use the properly formatted postcode.
    $valid['postcode'] = $correctPostcode;
    return TRUE;
  }

  public function validateUKPostcodeByRegex(string $rawPostcode, array &$valid): bool {
    if (preg_match(self::VALID_UK_POSTCODE_REGEX, $rawPostcode)) {
      $valid['postcode'] = $rawPostcode;
      return TRUE;
    }
    return TRUE;
  }

  /**
   * Process a request
   *
   * Request data is just key, value pairs from the form data. If it does not
   * have 'token' field then a token is generated and returned. Otherwise the
   * token is checked and processing continues.
   *
   * @param \Civi\Inlay\ApiRequest $request
   * @return array
   *
   * @throws \Civi\Inlay\ApiException;
   */
  public function processRequest(ApiRequest $request): array {
    $need = $request->getBody()['need'];
    if ($need === 'prefillData') {
      return $this->prefillDataRequest($request->getBody());
    }
    elseif ($need === 'upToDateCount') {
      return $this->upToDateCountRequest();
    }
    elseif ($need === 'submitData') {
      return $this->submitDataRequest($request);
    }
    else {
      return ['error' => 'Invalid request'];
    }
  }

  protected function prefillDataRequest(array $requestBody): array {
    $output = ['inputs' => []];
    $contactID = $this->getChecksummedContactID($requestBody);
    if (!$contactID) {
      Civi::log()->debug("InlayPetition prefillDataRequest: not returning data for ct $contactID - checksum mismatch");
      return ['ok' => 1];
    }

    $data = compact('contactID');
    $data['activitySubject'] = $this->getName()
      . (empty($data['variation']) ? '' : (' ~ ' . $data['variation']));
    if ($this->contactAlreadySigned($data)) {
      // Don't return personal data twice.
      Civi::log()->debug("InlayPetition prefillDataRequest: not returning data, ct $contactID already signed");
      return ['ok' => 1];
    }

    $contact = Contact::get(FALSE)
      ->addWhere('id', '=', $contactID)
      ->addSelect('first_name', 'last_name', 'email_primary.email', 'address_primary.postal_code')
      ->execute()->first();
    if ($contact) {
      $output['inputs']['first_name'] = $contact['first_name'] ?? '';
      $output['inputs']['last_name'] = $contact['last_name'] ?? '';
      $output['inputs']['email'] = $contact['email_primary.email'] ?? '';
      if ($this->config['askPostcode'] !== 'no') {
        $output['inputs']['postcode'] = $contact['address_primary.postal_code'] ?? '';
      }
      // Civi::log()->debug("InlayPetition prefillDataRequest: returning data for ct $contactID (disabled, returning nothing)");
      // return ['ok' => 2];
      Civi::log()->debug("InlayPetition prefillDataRequest: returning data for ct $contactID");
      return $output;
    }
    else {
      Civi::log()->debug("InlayPetition prefillDataRequest: not returning data, contact not found.");
      return ['ok' => 1];
    }
  }

  /**
   * Return a cached count of signatures.
   *
   * This may be up to 1 minute out of date.
   */
  protected function upToDateCountRequest(): array {
    return $this->getCounts(60);
  }

  /**
   * Get counts, optionally guaranteed to be up to date within the given parameter.
   */
  protected function getCounts(?int $maxSecondsOld = NULL): array {
    /** @var \CRM_Utils_Cache_CacheWrapper */
    $cache = Civi::cache('inlaypetition');
    $cacheKey = 'counts' . $this->getID();
    $counts = $cache->get($cacheKey);

    $needRecount = !is_array($counts) || (
      is_int($maxSecondsOld)
      && ($counts['countedAt'] ?? 0) < time() - $maxSecondsOld
    );
    if (!$needRecount) {
      // $age = time() - $counts['countedAt'];
      // Civi::log()->debug("Using $cacheKey $age seconds old, total $counts[count]");
      return $counts;
    }

    // (re)Count people signed up...
    $subject = $this->getName();
    $data = [
      'count' => 0,
      // 'variationCounts' => [],
      // 'lastSigner' => ['when' => '', 'who' => ''],
    ];

    if ($this->config['allowVariations']) {
      // We need to output counts for each variation.
      // Variations are stored in the subject, made of {inlayName}<space>~<space>{variationName}
      $activities = \Civi\Api4\Activity::get(FALSE)
        ->addSelect('subject', 'COUNT(id) AS count')
        ->addWhere('activity_type_id', '=', $this->getActivityTypeID())
        ->addWhere('subject', 'LIKE', "$subject ~ %")
        ->addGroupBy('subject')
        ->execute();
      $variationCounts = [];
      $offset = strlen($subject) + 3;
      $total = 0;
      foreach ($activities as $activity) {
        $variationName = substr($activity['subject'], $offset);
        $variationCounts[$variationName] = (int) $activity['count'];
        $total += (int) $activity['count'];
      }
      $data['count'] = $total;
      $data['variationCounts'] = $variationCounts;
    }
    else {
      $total = \Civi\Api4\Activity::get(FALSE)
        ->addSelect('COUNT(*) AS count', 'MAX(id) AS last_id')
        ->addWhere('activity_type_id', '=', $this->getActivityTypeID())
        ->addWhere('subject', '=', $subject)
        ->execute()->first();
      $data['count'] = $total['count'];

      // Look up last signer
      if ($total['last_id']) {
        $signer = \Civi\Api4\Activity::get(FALSE)
          ->addSelect('activity_date_time', 'contact.first_name')
          ->addJoin('Contact AS contact', 'LEFT', 'ActivityContact', ['contact.record_type_id', '=', 3])
          ->addWhere('id', '=', $total['last_id'])
          ->execute()->first();
        if ($signer) {
          $data['lastSigner'] = [
            'when' => strtotime($signer['activity_date_time']),
            'who' => $signer['contact.first_name'],
          ];
        }
      }
    }

    // Cache this.
    $data['countedAt'] = time();
    // Civi::log()->debug("Caching $cacheKey ", $data);
    $cache->set($cacheKey, $data, self::MAX_COUNT_AGE_SECONDS);
    return $data;
  }

  protected function getChecksummedContactID(array $requestBody): int {
    $contactID = $requestBody['ctID'] ?? 0;
    $checksum = $requestBody['cs'] ?? '';
    if (\CRM_Contact_BAO_Contact_Utils::validChecksum($contactID, $checksum)) {
      return $contactID;
    }
    return 0;
  }

  protected function submitDataRequest(ApiRequest $request): array {

    Civi::log()->debug('Petition inlay request: ' . json_encode($request->getBody()));
    $data = $this->cleanupInput($request->getBody());

    if (empty($data['token'])) {
      // Unsigned request. Issue a token that will be valid in 2s time and lasts 2mins max.
      return ['token' => $this->getCSRFToken(['data' => $data, 'validFrom' => 2, 'validTo' => 120])];
    }

    if ($this->config['useQueue']) {
      // Defer processing the data to a queue. This speeds things up for the user
      // and avoids database deadlocks.
      /** @var \CRM_Queue_Queue $queue */
      $queue = static::getQueueService();

      // We have context that is not stored in $data, namely which Inlay Instance we are.
      // Store that now.
      $data['inlayID'] = $this->getID();

      /** @var \CRM_Queue_Queue $queue */
      $queue->createItem(new CRM_Queue_Task(
        // callback
        ['Civi\\Inlay\\Petition', 'processQueueItem'],
        // arguments
        [$data],
        // title
        ""
      ));
      // Civi::log()->debug("Queued item", $data);
    }
    else {
      // Immediate processing.
      $this->processDeferredSubmission($data);
    }

    return ['success' => 1];
  }

  /**
   * @return \CRM_Queue_Service
   */
  public static function getQueueService($failed = FALSE): CRM_Queue_Queue {
    return CRM_Queue_Service::singleton()->create([
      'type'  => 'Sql',
      'name'  => 'inlay-petition' . ($failed ? '-failed' : ''),
      // We do NOT want to delete an existing queue!
      'reset' => FALSE,
    ]);
  }

  /**
   * Process a queued submission.
   *
   * This is the callback for the queue runner.
   *
   * Nb. the data has already been validated.
   *
   * @param mixed?
   * @param array
   *
   * @return bool TRUE if it went ok, FALSE will prevent further processing of the queue.
   */
  public static function processQueueItem($queueTaskContext, $data) {

    try {
      // Get instance ID.
      $id = (int) $data['inlayID'];

      // Get the Inlay Object from database if we don't have it cached.
      if (($id > 0) && !isset(static::$instanceCache[$id])) {
        $inlayData = \Civi\Api4\Inlay::get(FALSE)
          ->addWhere('id', '=', (int) $data['inlayID'])
          ->execute()->first();
        $inlay = new static();
        $inlay->loadFromArray($inlayData);
        // Store on cache.
        static::$instanceCache[$id] = $inlay;
      }

      // Error if we couldn't find it.
      if (empty(static::$instanceCache[$id])) {
        throw new \RuntimeException("Invalid Inlay/Petition queue item, failed to load instance.");
      }

      // Finally, use it to process the data.
      static::$instanceCache[$id]->processDeferredSubmission($data);
    }
    catch (\Exception $e) {
      // re-queue errors
      \Civi::log()->error("Error processing Inlay Petition queue item: " . $e->getMessage() . " Moving to failed queue.", ['exception' => $e]);
      $queue = static::getQueueService(TRUE);
      /** @var \CRM_Queue_Queue $queue */
      $queue->createItem(new CRM_Queue_Task(
        ['Civi\\Inlay\\Petition', 'processQueueItem'],
        [$data],
        $e->getMessage()
      ));
    }

    // Move on to next item in queue.
    return TRUE;
  }

  /**
   * Process a submission (either from the queue or a real-time submission)
   *
   * This is where the bulk of the work is done.
   *
   * - identify/create contactID
   * - if opt-in, add to group and note whether this was a change.
   * - reset on_hold for given email and set it to is_bulkmail
   * - add Signed Petition activity, including opt-in data
   *     - subject: {inlay name}[ ~ {variation}]
   * - send configured thank you email.
   *
   * @var array $data
   */
  public function processDeferredSubmission($data) {

    Civi::log()->debug('processDeferredSubmission: ' . json_encode($data));
    $pipeline = [
      'findOrCreateContact'  => [$this, 'findOrCreateContact'],
      'ensureEmailNotOnHold' => [$this, 'ensureEmailNotOnHold'],
      'updatePostcode'       => [$this, 'updatePostcode'],
      'handleSignup'         => [$this, 'handleSignup'],
      'recordActivity'       => [$this, 'recordActivity'],
      'sendThanksEmail'      => [$this, 'sendThanksEmail'],
    ];
    // Allow other code to alter the pipeline of callables.
    $event = GenericHookEvent::create(['pipeline' => &$pipeline]);
    Civi::dispatcher()->dispatch(self::EVENT_ALTER_PIPELINE, $event);

    foreach ($pipeline as $callable) {
      $callable($this, $data);
    }
  }

  /**
   * Ensures we have contactID in the $data array.
   */
  public function findOrCreateContact(Petition $inlay, array &$data) {
    if (!empty($data['contactID'])) {
      // From checksum.
      $contactID = $data['contactID'];
      // @todo update data with new data.
    }
    else {
      // Find Contact with XCM.
      $params = [
        'contact_type' => 'Individual',
        'email'        => $data['email'],
        'first_name'   => $data['first_name'],
        'last_name'    => $data['last_name'],
      ];
      $contactID = civicrm_api3('Contact', 'getorcreate', $params)['id'] ?? NULL;
    }
    if (!$contactID) {
      Civi::log()->error('Failed to getorcreate contact with params: ' . json_encode($params));
      throw new \Civi\Inlay\ApiException(500, ['error' => 'Server error: XCM1']);
    }
    $data['contactID'] = $contactID;
  }

  public function ensureEmailNotOnHold(Petition $inlay, array &$data) {

    // Since they gave their email, let's make sure it's not on hold.
    $updatedEmails = \Civi\Api4\Email::update(FALSE)
      ->addWhere('contact_id', '=', $data['contactID'])
      ->addWhere('email', '=', $data['email'])
      ->addWhere('on_hold', '>', 0)
      ->addValue('on_hold', 0)
      ->addValue('reset_date', 'now')
      ->addOrderBy('is_primary', 'DESC')
      ->addOrderBy('is_bulkmail', 'DESC')
      ->execute();
    if ($updatedEmails->countFetched()) {
      Civi::log()->debug($updatedEmails->countFetched() . " email record(s) for {email} were on_hold and have now been released.", ['email' => $data['email']]);
    }

    //   // Ensure the first of these (in case of duplicates) is set bulkmail.
    //   $updatedEmails = \Civi\Api4\Email::update(FALSE)
    //     ->addWhere('id', '=', $updatedEmails->first()['id'])
    //     ->addWhere('is_bulkmail', '=', FALSE)
    //     ->addValue('is_bulkmail', TRUE)
    //     ->execute();
    //   if ($updatedEmails->countFetched()) {
    //     $emailUpdatesMsg .= "The (first) of these emails was set to is_bulkmail";
    //   }
    //   Civi::log()->info($emailUpdatesMsg);
    // }
  }

  /**
   * Adds 'inlaypetition_signer_optin' to $data, with one of the following values:
   *
   * - yes_in_already
   * - yes_added (only if mailingGroup + user opted-in)
   * - no_not_in
   * - no_in_already
   *
   */
  public function handleSignup(Petition $inlay, array &$data) {
    // Handle optin.
    $optinRecord = '';
    if (!empty($this->config['mailingGroup'])) {
      $optinMode = $this->config['optinMode'];

      $isInGroup = \Civi\Api4\GroupContact::get(FALSE)
        ->selectRowCount()
        ->addWhere('group_id', '=', $this->config['mailingGroup'])
        ->addWhere('contact_id', '=', $data['contactID'])
        ->addWhere('status', '=', 'Added')
        ->execute()->count() > 0;

      // If there was no optin (e.g. signup form)
      // or if the user actively checked/selected yes, then sign up.
      if (
        $optinMode === 'none'
        || ($data['optin'] ?? 'no') === 'yes'
      ) {

        // Ensure contact is in the group.
        if ($isInGroup) {
          // Already in group.
          $optinRecord = 'yes_in_already';
        }
        else {
          $optinRecord = 'yes_added';
          $this->addContactToGroup($data['contactID']);
        }
      }
      else {
        // Contact not to be added (or removed)
        $optinRecord = $isInGroup ? 'no_in_already' : 'no_not_in';
      }
    }
    // else {
    //   Civi::log()->debug("processDeferredSubmission: NOT adding {$data['contactID']} to group: no group configured");
    // }
    $data['inlaypetition_signer_optin'] = $optinRecord;
    Civi::log()->debug("inlaypetition_signer_optin", compact('optinRecord'));
  }

  /**
   * This is very crude.
   */
  public function updatePostcode(Petition $inlay, array &$data) {
    if (empty($data['postcode'])) {
      return;
    }

    if (
      Address::get(FALSE)
        ->selectRowCount()
        ->addWhere('contact_id', '=', $data['contactID'])
        ->addWhere('postal_code', '=', $data['postcode'])
        ->execute()->countMatched() == 0
    ) {

      // We don't have an address with this postcode. Create one now.
      Address::create(FALSE)
        ->addValue('contact_id', $data['contactID'])
        ->addValue('postal_code', $data['postcode'])
        ->addValue('is_primary', 1)
        ->execute();
    }
  }

  /**
   * Creates an activity, except in the case of a petition signer who has already signed.
   *
   * Writes activity ID to either 'activityAdded' or 'activityFound' in $data.
   */
  public function recordActivity(Petition $inlay, array &$data) {

    // Write an activity. Calculate the subject here as we need it in a few places.
    $data['activitySubject'] = $this->getName()
      . (empty($data['variation']) ? '' : (' ~ ' . $data['variation']));

    if ($this->config['uxMode'] === 'petition') {
      $alreadySigned = $this->contactAlreadySigned($data);
      if ($alreadySigned) {
        // For petition, only record new activity if not done before.
        $data['activityFound'] = $alreadySigned;
        // Civi::log()->debug("Already signed", $data);
        return;
      }
    }

    $values = [
      'activity_type_id:name' => 'inlay_petition',
      'target_contact_id'     => $data['contactID'],
      'subject'               => $data['activitySubject'],
      'status_id'             => 'Completed',
      'source_contact_id'     => $data['contactID'],
      'location'              => $data['location'] ?? '',
      // 'details'            => $details,
    ];
    if ($data['inlaypetition_signer_optin']) {
      $values['inlaypetition_signer.inlaypetition_signer_optin'] = $data['inlaypetition_signer_optin'];
    }

    $result = Activity::create(FALSE)->setValues($values)->execute()->first();
    $data['activityAdded'] = $result['id'];
    // Civi::log()->debug("Signed", $data);
  }

  public function sendThanksEmail(Petition $inlay, array &$data) {
    if (empty($this->config['thanksMsgTplID'])) {
      return;
    }

    try {
      $message = new \CRM_Inlaypetition_WorkflowMessage_ThanksForSigning();
      $message->setPetitionNameHTML(htmlspecialchars($this->config['publicTitle']));
      $message->setCustomThanksHTML($this->config['thanksEmailCustomHTML'] ?? '');
      $message->setContactID($data['contactID']);
      list($domainFromName, $domainFromEmail) = \CRM_Core_BAO_Domain::getNameAndEmail(TRUE);
      $message->setFrom([
        'name' => $domainFromName,
        'email' => $domainFromEmail,
      ]);
      $message->setTo(['name' => "$data[first_name] $data[last_name]", 'email' => $data['email']]);
      [$success] = $message->sendTemplate([
        'messageTemplateID' => $this->config['thanksMsgTplID'],
      ]);
      if ($success) {
        Civi::log()->info("Sent message tpl {template_id} to {email}", ['email' => $data['email'], 'template_id' => $this->config['thanksMsgTplID']]);
      }
      else {
        throw new \RuntimeException('Unknown reason.');
      }

      // *   Ex: ['modelProps' => [...classProperties...]]
      // This was the original, using MessageTemplate.send API
      // $from = civicrm_api3('OptionValue', 'getvalue', ['return' => "label", 'option_group_id' => "from_email_address", 'is_default' => 1]);
      //
      // // We use the email send in the data, as that's what they'd expect.
      // $params = [
      //   'id'             => $this->config['thanksMsgTplID'],
      //   'from'           => $from,
      //   'to_email'       => $data['email'],
      //   // 'bcc'            => "forums@artfulrobot.uk",
      //   'contact_id'     => $contactID,
      //   'disable_smarty' => 1,
      // /*
      // 'template_params' =>
      // [ 'foo' => 'hello',
      // // {$foo} in templates 'bar' => '123',
      // // {$bar} in templates ],
      // ];
      // civicrm_api3('MessageTemplate', 'send', $params);
      //  */

      // This is the new one using Email.send from the EmailAPI extension, which also saves it as an activity.
      // $params = [
      //   'template_id'                  => $this->config['thanksMsgTplID'],
      //   'alternative_receiver_address' => $data['email'],
      //   // 'bcc'                       => "f1@artfulrobot.uk",
      //   'contact_id'                   => $data['contactID'],
      //   'disable_smarty'               => 1,
      //   'activity_details'             => 'tplName',
      // ];
      // civicrm_api3('Email', 'send', $params);
    }
    catch (\Exception $e) {
      // Log silently.
      Civi::log()->error(
        "Failed to send MessageTemplate {thanksMsgTplID} to {email}. Caught " . get_class($e) . ": " . $e->getMessage(),
        ['email' => $data['email'], 'thanksMsgTplID' => $this->config['thanksMsgTplID']]
          );
    }
  }

  /**
   * Has the given contact signed this petition already?
   *
   * @return int Activity ID or 0
   */
  public function contactAlreadySigned(array $data): int {

    $activityTypeID = $this->getActivityTypeID();

    $found = (int) \CRM_Core_DAO::singleValueQuery("
        SELECT a.id
        FROM civicrm_activity a
        INNER JOIN civicrm_activity_contact ac
        ON a.id = ac.activity_id
            AND ac.record_type_id = 3 /*target*/
            AND ac.contact_id = %1
        WHERE a.activity_type_id = %2 AND a.subject = %3
        LIMIT 1
      ", [
        1 => [$data['contactID'], 'Integer'],
        2 => [$activityTypeID, 'Integer'],
        3 => [$data['activitySubject'], 'String'],
      ]);

    return $found;
  }

  /**
   * Add the contact to the mailing group.
   *
   * @param int $contactID
   */
  public function addContactToGroup($contactID) {
    $contacts = [$contactID];
    $groupID = $this->config['mailingGroup'];
    \CRM_Contact_BAO_GroupContact::addContactsToGroup($contacts, $groupID, 'Petition', $status = 'Added');
  }

  /**
   * Validate and clean up input data.
   *
   * Possible outputs:
   * - first_name
   * - last_name
   * - email
   * - location (URL)
   * - optin
   * - token TRUE|unset
   * - postcode
   * - variation
   *
   * @param array $data with keys:
   *    - email
   *    - first_name
   *    - last_name
   *    - ?location
   *    - ?optin
   *    - ?variation
   *    - ?token
   *
   * @return array
   */
  public function cleanupInput($data) {
    /** @var Array errors in this array, it will later be converted to a string. */
    $errors = [];
    /** @var Array Collect validated data in this array */
    $valid = [];

    // Check we have what we need.
    foreach (['first_name', 'last_name', 'email'] as $field) {
      $val = trim($data[$field] ?? '');
      if (empty($val)) {
        $errors[] = str_replace('_', ' ', $field) . " required";
      }
      else {
        if ($field === 'email' && !filter_var($val, FILTER_VALIDATE_EMAIL)) {
          $errors[] = "invalid email address";
        }
        else {
          $valid[$field] = $val;
        }
      }
    }

    if ($this->config['askPostcode'] === 'uk') {
      if ($data['postcode'] === 'N/A') {
        // We allow 'N/A' because reasons...
        $valid['postcode'] = $data['postcode'];
      }
      else {
        // We expect a valid UK postcode.
        if (!$this->validateUKPostcode($data['postcode'], $valid)) {
          $errors[] = "Invalid UK postcode.";
        }
      }
    }
    elseif ($this->config['askPostcode'] === 'other') {
      if (preg_match('/^[a-zA-Z0-9.+./"\'()) ]{1,64}$/', $data['postcode'])) {
        $valid['postcode'] = $data['postcode'];
      }
      else {
        $errors[] = "Invalid postal code.";
      }
    }

    if ($errors) {
      throw new \Civi\Inlay\ApiException(400, ['error' => implode(', ', $errors)]);
    }

    // Clean up location
    $location = trim($data['location'] ?? '');
    if ($location) {
      $containsEmoji = (preg_match("/[\u{1f300}-\u{1f5ff}\u{e000}-\u{f8ff}]/u", $location));
      $looksFairEnough = preg_match('@^https?://[^/]+/[^<>]+$@', $location);
      if (!$containsEmoji && $looksFairEnough && strlen($location) < 255) {
        // Looks fine.
        $valid['location'] = $location;
      }
      else {
        // Hmmm looks dodgy.
        $valid['location'] = mb_substr(preg_replace('@[<>\u{1f300}-\u{1f5ff}\u{e000}-\u{f8ff}]+@u', '_', $location), 0, 200) . ' (cleaned)';
        Civi::log()->notice("Cleaned up dodgy location (received with petition submission with email: '$valid[email]'):\n"
          . "Dodgy  : $location\n"
          . "Cleaned: $valid[location]");
      }
    }

    // Clean up variation
    $variation = trim($data['variation'] ?? '');
    Civi::log()->info('inlaypetition data ' . json_encode($data, JSON_PRETTY_PRINT));
    if ($this->config['allowVariations']) {
      // Require a variation, but munge falsy to 'default'
      $variation = $variation ?: 'default';
      if (preg_match("@^[a-zA-Z0-9,.'/ -]{1,255}$@", $variation)) {
        $valid['variation'] = $variation;
      }
      else {
        throw new \Civi\Inlay\ApiException(400, ['error' => 'Invalid petition variation']);
      }
    }
    elseif ($variation) {
      // Variation sent but not allowed.
      throw new \Civi\Inlay\ApiException(400, ['error' => 'Petition is not configured to allow variations.']);
    }

    // Optin.
    switch ($this->config['optinMode']) {
      case 'radios':
        // We require the 'optin' data.
        if (!in_array($data['optin'] ?? '', ['yes', 'no'])) {
          // Error.
          throw new \Civi\Inlay\ApiException(400, ['error' => 'Please choose to opt-in to updates or not']);
        }
        $valid['optin'] = $data['optin'];
        break;

      case 'checkbox':
        if (($data['optin'] ?? '') === 'yes') {
          $valid['optin'] = 'yes';
        }
        else {
          // Checkboxes don't submit 'no' for off.
          $valid['optin'] = 'no';
        }
        break;

      default:
        $valid['optin'] = 'yes';
    }

    $contactID = $this->getChecksummedContactID($data);
    if ($contactID) {
      $valid['contactID'] = $contactID;
    }

    // Data is valid.
    if (!empty($data['token'])) {
      // There is a token, check that now.
      try {
        $this->checkCSRFToken($data['token'], $valid);
        $valid['token'] = TRUE;
      }
      catch (\InvalidArgumentException $e) {
        // Token failed. Issue a public friendly message, though this should
        // never be seen by anyone legit.
        Civi::log()->notice("Token error: " . $e->getMessage() . "\n" . $e->getTraceAsString());
        throw new \Civi\Inlay\ApiException(
          400,
          ['error' => "Mysterious problem, sorry! Code " . substr($e->getMessage(), 0, 3)],
          $e->getMessage() /* provide full message as internalError */
              );
      }

      // Validation that is more expensive, and for fields where invalid data
      // would likely represent misuse of the form is done now - after the
      // token check, to avoid wasting server resources on spammers trying to
      // randomly post to the endpoint.

      /*
      if ($this->config['phoneAsk'] && !empty($data['phone'])) {
      // Check the phone.
      $valid['phone'] = preg_replace('/[^0-9+]/', '', $data['phone']);
      }
       */
    }

    return $valid;
  }

  /**
   * Get the Javascript app script.
   *
   * This will be bundled with getInitData() and some other helpers into a file
   * that will be sourced by the client website.
   *
   * @return string Content of a Javascript file.
   */
  public function getExternalScript(): string {
    return file_get_contents(E::path('src/alpinejs3-intersect.js'))
      . file_get_contents(E::path('src/alpinejs3.js'))
      . file_get_contents(E::path('src/inlay-progress-v1.0.1.js'))
      . file_get_contents(E::path('src/inlay-socials-v1.1.1.js'))
      . file_get_contents(E::path('src/inlay-petition.js'));
  }

  /**
   * Return a list of assets used by this Inlay.
   *
   * We do not need to include our bundle file; Inlay itself looks after that.
   */
  public function getAssets(): array {
    return ['inlaypetition_' . $this->getID()];
  }

}
