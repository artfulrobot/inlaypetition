(function (angular, $, _) {
  angular.module("inlaypetition").config(function ($routeProvider) {
    $routeProvider.when("/inlay/petition/:id", {
      controller: "InlaypetitionPetition",
      controllerAs: "$ctrl",
      templateUrl: "~/inlaypetition/Petition.html",

      // If you need to look up data when opening the page, list it out
      // under "resolve".
      resolve: {
        various: function ($route, crmApi4, $route) {
          const params = {
            inlayTypes: ["InlayType", "get", {}, "class"],
            groups: [
              "Group",
              "get",
              {
                select: ["id", "title"],
                where: [
                  ["group_type", "=", 2],
                  ["is_active", "=", true],
                  ["is_hidden", "=", false],
                ],
                orderBy: { title: "ASC" },
              },
            ],
            messageTpls: [
              "MessageTemplate",
              "get",
              {
                select: ["id", "msg_title", "msg_subject"],
                where: [
                  ["is_active", "=", true],
                  ["is_sms", "=", false],
                  ["workflow_id", "IS NULL"],
                ],
                orderBy: { msg_title: "ASC" },
              },
              "id",
            ],
          };
          if ($route.current.params.id > 0) {
            params.inlay = [
              "Inlay",
              "get",
              { where: [["id", "=", $route.current.params.id]] },
              0,
            ];
            params.image = [
              "InlayAsset",
              "get",
              { identifier: "inlaypetition_" + $route.current.params.id },
            ];
          }
          return crmApi4(params);
        },
      },
    });
  });

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  angular
    .module("inlaypetition")
    .controller(
      "InlaypetitionPetition",
      function ($scope, crmApi4, crmStatus, crmUiHelp, various) {
        // The ts() and hs() functions help load strings for this module.
        var ts = ($scope.ts = CRM.ts("inlaypetition"));
        var hs = ($scope.hs = crmUiHelp({
          file: "CRM/inlaypetition/Petition",
        })); // See: templates/CRM/inlaypetition/Petition.hlp
        // Local variable for this controller (needed when inside a callback fn where `this` is not available).

        $scope.imageUploadInputValue = null;
        $scope.mailingGroups = various.groups;
        $scope.image = various.image;
        $scope.inlayType = various.inlayTypes["Civi\\Inlay\\Petition"];
        console.log({ various }, $scope.inlayType);
        $scope.mailingGroups = various.groups;
        $scope.messageTpls = various.messageTpls;

        if (various.inlay) {
          $scope.inlay = various.inlay;
        } else {
          $scope.inlay = {
            class: "Civi\\Inlay\\Petition",
            name: "New " + $scope.inlayType.name,
            public_id: "new",
            id: 0,
            config: JSON.parse(JSON.stringify($scope.inlayType.defaultConfig)),
          };
        }
        const inlay = $scope.inlay;

        // Define all the networks we support here.
        const knownSocials = {
          bluesky: "Bluesky",
          copy: "Copy Link",
          email: "Email",
          facebook: "Facebook",
          linkedin: "LinkedIn",
          mastodon: "Mastodon",
          threads: "Threads",
          whatsapp: "WhatsApp",
          twitter: "X/Twitter",
        };
        $scope.smShares = [];
        inlay.config.socials.forEach((sm) => {
          $scope.smShares.push({
            active: true,
            name: sm,
            label: knownSocials[sm],
          });
          delete knownSocials[sm];
        });
        Object.keys(knownSocials).forEach((sm) => {
          $scope.smShares.push({
            active: false,
            name: sm,
            label: knownSocials[sm],
          });
        });

        $scope.smActive = function (sm) {
          return $scope.smShares.find((x) => x.name === sm).active;
        };

        $scope.save = function (andThen) {
          // Re-form the socials part of inlay.
          inlay.config.socials = [];
          $scope.smShares.forEach((sm) => {
            if (sm.active) {
              inlay.config.socials.push(sm.name);
            }
          });
          console.log("Saving " + JSON.stringify(inlay));

          return crmStatus(
            // Status messages. For defaults, just use "{}"
            { start: ts("Saving..."), success: ts("Saved") },
            // The save action. Note that crmApi() returns a promise.
            crmApi4("Inlay", "save", { records: [inlay] })
          ).then((r) => {
            if (andThen === "preview") {
              window.location = CRM.url("civicrm/inlay/use", { id: inlay.id });
            } else if (andThen === "backToList") {
              window.location = CRM.url("civicrm/a?#inlays");
            } else {
              // Update the inlay ID - in case it was a new one.
              inlay.id = r[0].id;
              inlay.public_id = r[0].public_id;
            }
          });
        };

        $scope.uploadNewImageChanged = function (e) {
          const filesInput = document.getElementById("imageFileUploadInput");
          if (filesInput.files.length === 1) {
            $scope.uploadNewImage({ preventDefault() {} });
          }
        };
        $scope.uploadNewImage = async function (e) {
          e.preventDefault();
          if (!inlay.id) {
            if (
              confirm(
                "You need to save this inlay first before uploading an image. Do that now?"
              )
            ) {
              await $scope.save();
            }
            if (!inlay.id) {
              alert("Something went wrong, unable to save.");
              return;
            }
          }
          const filesInput = document.getElementById("imageFileUploadInput");
          if (filesInput.files.length !== 1) {
            alert("You need to select a file first.");
            return;
          }

          d = {
            identifier: "inlaypetition_" + inlay.id,
            filename: filesInput.files[0].name,
            data: "",
          };

          var p = new Promise((resolve, reject) => {
            if (filesInput.files.length === 1) {
              var fr = new FileReader();
              fr.addEventListener("load", (e) => {
                // File loaded.
                console.log("File read ok");
                d.data = fr.result;
                resolve(d);
              });
              fr.readAsDataURL(filesInput.files[0]);
            }
          });
          p.then((d) => {
            return crmStatus(
              // Status messages. For defaults, just use "{}"
              { start: ts("Uploading..."), success: ts("Done") },
              // The save action. Note that crmApi() returns a promise.
              crmApi4("InlayAsset", "put", d)
            ).then((r) => {
              console.log("upload result", r);
              $scope.image = r;
              filesInput.value = null;
            });
          });
        };

        $scope.removeImage = function (e) {
          e.preventDefault();
          if (!inlay.id) {
            // doesn't make sense.
            return;
          }
          if (!confirm("Delete the image?")) {
            return;
          }
          d = {
            identifier: "inlaypetition_" + inlay.id,
          };

          crmStatus(
            // Status messages. For defaults, just use "{}"
            { start: ts("Deleting Image..."), success: ts("Done") },
            crmApi4("InlayAsset", "delete", {
              identifier: "inlaypetition_" + inlay.id,
            })
          ).then(() => {
            console.log("clearing scope.image");
            $scope.image = null;
          });
        };
      }
    );
})(angular, CRM.$, CRM._);
